extends: post.liquid
title: New Design
date: 09 Jul 2017 15:08:00 +0100
path: /:year/:month/:day/new-design
---

I decided it is time for a new design for this site.
I went with a minimal design I adopted from [booyaa][] (which he adopted from [johann][]).
Instead of fixing my broken Jekyll setup, I switched to [cobalt][], a static site generator written in Rust.

I migrated all content and fixed the layout where necessary.
Links _should_ be stable, but if not please let me know.

Maybe this gets me more into writing again.

[cobalt]: https://github.com/cobalt-org/cobalt.rs
[booyaa]: https://booyaa.wtf/
[johann]: http://johannh.me/
