extends: post.liquid
title: try.redis.io is online
date: 10 Jan 2013 21:02:00 +0100
path: /:year/:month/:day/try-redis-io-is-online
---
Finally:

## [try.redis.io][tryredis]

It's back online. [@antirez][] changed the DNS to point to my server, so it's official. Use it, tell others, [contribute][], learn.


[tryredis]: http://try.redis.io
[@antirez]: https://github.com/antirez/
[contribute]: https://github.com/badboy/try.redis
