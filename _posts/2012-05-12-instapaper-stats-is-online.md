extends: post.liquid
title: instapaper-stats is online
date: 12 May 2012 17:13:00 +0200
path: /:year/:month/:day/instapaper-stats-is-online
---

I mentioned the script I'm using for my instapaper stats [here](http://fnordig.de/2012/05/08/my-instapaper-stats/).
And it's now online: [instapaper-stats](https://github.com/badboy/instapaper-stats)
