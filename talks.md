extends: simple.liquid
title: Talks
route: talks
path: /talks
---

I gave some talks in the past and I'd love to give some more in the future.
I will talk about Redis, Web Development and Rust stuff or about any of the project I'm involved in.
Want me to speak? Contact me [via mail](mailto:janerik@fnordig.de).

## 2017

I am up for more speaking gigs this year. Contact me [via mail](mailto:janerik@fnordig.de).

* **WebAssembly for the rest of us**, Codemotion Amsterdam, 16.-17.05.2017, ([Slides](http://www.hellorust.com/codemotion-ams/slides/), [Conference](http://amsterdam2017.codemotionworld.com/talk-detail/?detail=5162))
* **WebAssembly for the rest of us**, Mozilla Dev Roadshow (Düsseldorf), 14.05.2017, ([Slides](http://www.hellorust.com/codemotion-ams/slides/), [Meetup](https://beyondtellerrand.com/events/duesseldorf-2017/side-events#mozilla-roadshow))
* **Learn you some Rust** (workshop), Booster Conf, 15.-17.03.2017, ([Material](http://hellorust.com/booster), [Conference](https://www.boosterconf.no/talks/836))
* **Rust and Ruby - make it fast**, Cologne.rb/Rust Cologne joint meetup, 18.01.2017 ([Slides](https://badboy.github.io/rust-and-ruby/), [Meetup](http://www.colognerb.de/events/januar-meetup-2017))

## 2016

* **Hire me for the JS, keep me for the Rust** (workshop), Rust Belt Rust (Pittsburgh, PA, USA), 27./28.10.2016 ([Conference](http://www.rust-belt-rust.com/sessions/), [Material](http://www.hellorust.com/emscripten/))
* **Compiling Rust to your Browser**, Rust Cologne/Bonn, 05.09.2016 ([Slides](https://badboy.github.io/rust-to-the-browser/), [Resources](http://www.hellorust.com/emscripten/), [Meetup](https://www.meetup.com/Rust-Cologne-Bonn/events/233139845/), [Video](https://media.ccc.de/v/rustmcb.2016.09.compiling-rust-to-asmjs))
* **Rust from the back to the front**, Rust Stockholm, 29.08.2016 ([Slides](https://badboy.github.io/rust-sthlm/#1), [Meetup](https://www.meetup.com/ruststhlm/events/232054490/))
* **Webdevelopment with Rust** - Rust Amsterdam, 02.03.2016 ([Slides](http://badboy.github.io/webdev-rust-slides/), [Meetup](http://www.meetup.com/Rust-Amsterdam/events/227827508/))
* **Automatic Crate publishing – semantic.rs** - Rust Cologne/Bonn, 03.02.2016 ([Slides](http://badboy.github.io/semantic-rs-slides/#1))

## 2015

* **Hire me for the JS, keep me for the Rust** - Hungarian Webconference, 14.11.2015 ([Video](https://www.youtube.com/watch?v=L9sTIi7wFPo))
* **Redis Cluster** - PHPUGDUS, 28.05.2015 ([Slides](http://fnordig.de/talks/2015/phpugdus/redis-cluster/slides.pdf), [Slides on slidr.io](http://slidr.io/badboy/redis-cluster))
* **U2F Authentication** - Cologne.rb, 20.05.2015 ([Slides](https://fnordig.de/talks/2015/colognerb/u2f/slides.pdf), [Slides on slidr.io](http://slidr.io/badboy/u2f-authentication))
* **Rust Introduction** - CCCAC, 05.03.2015 ([Video](https://videoag.fsmpi.rwth-aachen.de/?view=player&lectureid=4532), [Slides](https://fnordig.de/talks/2015/cccac/rust-intro/#0))

## 2014

* **Introduction to Redis** - OpenTechSchool Dortmund, 16.12.2014 - these slides are actually extracted from the _real slides_, which were built in [try.redis.io](http://try.redis.io). ([Slides](http://fnordig.de/talks/2014/ots/redis-introduction-otsdo-2014-12-16.pdf))
* **Rust for Rubyists** - FrOSCon, RedFrogConf, 24.08.2014 - this was a bit spontaneous and prepared in only one day. Mostly the content from [first experience with Rust](/2014/08/12/first-experience-with-rust/). I also gave this talk two months later at the Barcamp Salzburg (again, totally spontaneous to fill a gap in the schedule). ([Slides](https://fnordig.de/talks/2014/froscon/rust-for-rubyists/), [PDF](https://fnordig.de/talks/2014/froscon/rust-for-rubyists/froscon2014-redfrogconf-rust-for-rubyists.pdf))

