extends: simple.liquid
title: About me
route: about
path: /about
---

25 years old.  
Studied Computer Science at [RWTH Aachen](http://www.rwth-aachen.de/) and [UiS](http://www.uis.no/) in Stavanger, Norway.  
Working at [rrbone](https://www.rrbone.net/de/).  
Programming a lot of [Ruby](http://www.ruby-lang.org/en/) and [Rust](http://www.rust-lang.org/).  
Speaking about projects and topics I am interested in. See [my previous talks](/talks).

Organizer of [RustFest](http://www.rustfest.eu/),
Co-Organizer of the [Rust User Group Cologne/Bonn](http://rustaceans.cologne/),
Co-Organizer of [otsconf Dortmund](https://otsconf.com/),
coach at [OpenTechSchool Dortmund](http://www.opentechschool.org/dortmund/),
contributor to [Redis](http://redis.io) and maintainer of [hiredis](https://github.com/redis/hiredis) and several client libraries.



## Links

* Twitter: [@badboy\_](https://twitter.com/badboy_)
* GitHub: [@badboy](https://github.com/badboy)
* Pinboard: [badboy](http://pinboard.in/u:badboy)

## Projects

* [Compiling Rust to your Browser](http://www.hellorust.com/emscripten/) - Resources, demos & more for compiling Rust to asm.js & WebAssembly
* [semantic-rs](https://github.com/semantic-rs/semantic-rs) - Automatic crate publishing
* [rdb-rs](http://rdb.fnordig.de/) - fast and efficient RDB parsing utility, including full documentation on the RDB file format
* [hiredis](https://github.com/redis/hiredis) - the minimalistic C client for Redis, as well as wrapper libraries: [hiredis-rb](https://github.com/redis/hiredis-rb/) ([gem](https://rubygems.org/gems/hiredis)), [hiredis-node](https://github.com/redis/hiredis-node) ([npm](https://www.npmjs.com/package/hiredis)), [hiredis-py](https://github.com/redis/hiredis-py) ([pypi](https://pypi.python.org/pypi/hiredis/))
* [try.redis.io](http://try.redis.io) - Online Redis REPL.
* [badbill](https://github.com/badboy/badbill) - A client for the [Billomat](http://www.billomat.com/en/api/) API
* [Redis FAQ](/redis-faq/) - an unofficial Redis FAQ with user questions

## Services

* [etherpad](https://pad.fnordig.de/) - real-time collaborative document editing
* [rezepte](http://rezepte.fnordig.de/) - a small database of recipes (german)

## Other

* [Events I've been at](http://badboy.hasbeen.at/)
* [Places I've been in](http://badboy.hasbeen.in/)
